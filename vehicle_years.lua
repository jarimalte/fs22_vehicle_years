-- This file is part of FS22_Vehicle_Years.
--
-- FS22_Vehicle_Years is free software: you can redistribute it and/or modify it
-- under the terms of the GNU General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- FS22_Vehicle_Years is distributed in the hope that it will be useful, but
-- WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
-- FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
-- details.
--
-- You should have received a copy of the GNU General Public License along with
-- FS22_Vehicle_Years. If not, see <https://www.gnu.org/licenses/>.

VehicleYears = {}

local function vyprint(value)
    if value ~= nil then
        print("VehicleYears: " .. value)
    else
        print("VehicleYears: nil")
    end
end

local function log_dbg(...)
    if VehicleYears.debugEnabled then
        vyprint(...)
    end
end

local function log_dbg_verbose(...)
    if VehicleYears.verboseDebugEnabled then
        vyprint(...)
    end
end

-- Copied and modified from https://stackoverflow.com/a/27028488
local function dump(o)
    if type(o) == 'table' then
        local s = '{ '
        for k, v in pairs(o) do
            if type(k) ~= 'table' then
                if type(k) ~= 'number' then
                    k = '"' .. k .. '"'
                end
                s = s .. '[' .. k .. '] = ' .. tostring(v) .. ','
            end
        end
        return s .. '} '
    else
        return tostring(o)
    end
end

---Returns the year of a vehicle based on category, brand and name
-- The Categories are values such as animalTransport and baleWrappers
-- @return Year of the vehicle if found otherwise nil
local function loadYearFromXML(category, brand, name, isMod)
    local xmlFile = nil
    local year = nil

    if isMod then
        xmlFile = XMLFile.load("vehicleYearsXMLMods", VehicleYears.modXmldatapath)
    else
        xmlFile = XMLFile.load("vehicleYearsXML", VehicleYears.xmldatapath)
    end

    if xmlFile ~= nil then
        -- Lookup iterate on all vehicles where category and brand matches and
        -- compare all names to find the one matching
        xmlFile:iterate(string.format("vehicles.%s.%s.vehicle", category, brand),
            function(_, key)
                if year == nil then
                    local local_name = g_i18n:convertText(xmlFile:getString(key .. ".name"))

                    log_dbg(string.format("key: %s", key))
                    log_dbg(string.format("name: %s", name))
                    log_dbg(string.format("local_name: %s", local_name))

                    if local_name == name then
                        year = xmlFile:getString(key .. ".year")
                    end
                end
            end)

        log_dbg(string.format("year: %s", year))

        -- Vehicles with unknown years are stored as "TBD"
        if year == "TBD" then
            -- Tread TBD as nil to not apply the year in-game
            year = nil
        end

        xmlFile:delete()
    end

    return year
end

---Returns the year of a vehicle based on the mod's XML file

-- @return Year of the vehicle if found otherwise nil
local function loadYearFromMod(xmlFilePath)
    log_dbg("loadYearFromMod")

    local year = nil

    if xmlFilePath ~= nil then
        local xmlFile = XMLFile.load("TempXML", xmlFilePath)

        if xmlFile ~= nil then
            year = xmlFile:getInt("vehicle.storeData.year")

            -- Vehicles with unknown years are stored as "TBD"
            if year == "TBD" then
                -- Tread TBD as nil to not apply the year in-game
                year = nil
            end

            log_dbg("Year loaded from mod: " .. year)

            xmlFile:delete()
        end
    end

    return year
end

---Callback for loading the "year" spec
function VehicleYears.loadYear(xmlFile, customEnvironment, baseDir)
    log_dbg("VehicleYears.loadYear")

    -- This year does not seem to be used or store in any way...
    -- Keep for now in case we figure out a usecase for it
    return nil
end

---Callback for getting the "year" spec
-- This will use the storeItem to lookup the vehicle in our database
-- @return The year of the vehicle if found otherwise nil
function VehicleYears.getYear(storeItem, realItem)
    log_dbg("VehicleYears.getYear")
    log_dbg_verbose(dump(storeItem))
    log_dbg_verbose(dump(storeItem.specs))

    log_dbg(string.format("Vehicle: category %s, brand %s, name %s",
            storeItem.categoryName, storeItem.brandNameRaw, storeItem.name))

    log_dbg(storeItem.specs.year)

    -- We store the result in the storeItem - If it is non-nil it means that we
    -- have already stored it, and do not need to lookup in the XML file again
    if storeItem.specs.year == nil then
        if storeItem.isMod then
            -- Try to read the year from the mod XML files for the vehicle first
            storeItem.specs.year = loadYearFromMod(storeItem.xmlFilename)
        end

        if storeItem.specs.year == nil then
            storeItem.specs.year = loadYearFromXML(storeItem.categoryName,
                storeItem.brandNameRaw, storeItem.name, storeItem.isMod)
        end
    end

    -- If this is nil, it won't show up in the spec list
    return storeItem.specs.year
end

-- Do initialization if not already done - Ensured to only run once
if VehicleYears.modActivated == nil then
    g_gui:loadProfiles(g_currentModDirectory .. "guiProfiles.xml")

    -- Adding year as a spec allows us to show the value when browsing vehicles
    g_storeManager:addSpecType("year", "shopListAttributeIconDate", nil,
        VehicleYears.getYear, "vehicle")

    VehicleYears.modActivated = true
    VehicleYears.debugEnabled = false
    VehicleYears.verboseDebugEnabled = false
    VehicleYears.xmldatapath = g_currentModDirectory .. "data/vehicle_years.xml"
    VehicleYears.modXmldatapath = g_currentModDirectory .. "data/vehicle_years_mods.xml"

    log_dbg("initialized")
end
